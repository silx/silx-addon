# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "17/07/2017"

from Orange.widgets.widget import OWWidget
from silx.gui.plot.PlotWindow import PlotWindow


class Plot1D(OWWidget):
    """
    Simple class to show content of a numpy array throw different views
    """
    name = "silx plot 1D"
    icon = "icons/plot-widget.svg"
    want_main_area = True

    priority = 9

    category = "silx"
    keywords = ["plot", "fit", "data", "1D"]

    inputs = [("data", dict, "addCurve")]
    outputs = []

    def __init__(self):
        super().__init__()

        self._plot = PlotWindow()
        self._plot.getFitAction().setVisible(True)
        self.controlArea.layout().addWidget(self._plot)

    def addCurve(self, data):
        """
        add a curve from the data dictionnary

        :param dict data: should contain 'x' and 'y' keys 
        """
        assert 'x' in data
        assert 'y' in data
        assert 'legend' in data
        self._plot.addCurve(x=data['x'], y=data['y'])

    def getPlot(self):
        """
        
        :return: the silx Plot object 
        """
        return self._plot


def main():
    from silx.gui import qt
    x = (0, 1, 2, 3, 5)
    y = (5, 4, 5, 6, 1)
    legend = 'testCurve'

    app = qt.QApplication([])
    s = Plot1D()
    s.addCurve({'x': x, 'y': y, 'legend': legend})
    s.show()
    app.exec_()


if __name__ == "__main__":
    main()