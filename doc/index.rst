============
Silx Add-ons
============

This Orange addon contains some of the silx gui.

Contents:

.. toctree::
   :maxdepth: 2
   :hidden:

   install.rst


:doc:`install`
    Installation

Widgets
-------

.. toctree::
   :maxdepth: 1

   widgets/dataviewer.rst
   widgets/plot1d.rst

