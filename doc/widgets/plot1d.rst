silx plot 1D
============


Signals
-------

**Inputs**:

- dictionary : data, must contains 'x', 'y' and 'name' keys.

Description
-----------

Simple widget displaying curves. This is based on the silx PlotWidget class.
