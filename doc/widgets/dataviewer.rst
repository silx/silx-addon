silx data viewer
================

Signals
-------

**Inputs**:

- numpy.ndarray : data. The viewer will created all possible views according to the numpy.ndarary dimension.

Description
-----------

Widget displaying views according to the input data. This is based on the silx DataViewer class.
